#!/usr/bin/env python2

import tracecmd
from ctracecmd import *
import atexit
from interface import implements, Interface #pip install python-interface



buckets = list()
timeIntervall = 0

def print_summarry(name, adjective):
    print("print_summarry")
    #print("bucket lenght", len(buckets))
    #print("all events counter for all buckets:", Collector().countAllEventsBuckets(buckets))
    #print("all events counter for bucket 1:", Collector().countAllEventsBucket(buckets[0]))
    #print("rcu events in all buckets:", Collector().countEventsByGroupBuckets("rcu"))
    #print("ext4 events in all buckets:", Collector().countEventsByGroupBuckets("ext4"))
    #print("rcu events bucket 1:", Collector().countEventsByGroupBucket(buckets[0], "rcu"))
    #print("list all events and counter:")
    #for name, event in Collector().listEventsCounterBuckets().items():
    #    print(name)
    #    print(event)
    #Collector().walk()
    
    
class IStatsCollector(Interface):
    def walk(self):
        pass
    
    def nextBucket(self, bucket):
        pass
    
    def nextTask(self, task):
        pass
    
    def account(self, system, name, number):
        pass

class Collector(implements(IStatsCollector)):

    def walk(self):
        for bucket in buckets:
            self.nextBucket(bucket)
            
    def nextBucket(self, bucket):
        print("NEW BUCKET AT", bucket)
        bucket.walk()
            
    def nextTask(self, task):
        print("NEXT TASK AT", task)
        task.walk()
            
    def account(self, system, name, number):
        print(system, name, number)
    
    """
    def countAllEventsBuckets(self, buckets):
        counter = 0
        for bucket in buckets:
            for t in bucket.taskDict.values():
                for ev in t.eventGroups.values():
                    for evCount in ev.events.values():
                        counter += evCount
        return counter
    
    def countAllEventsBucket(self, bucket):
        counter = 0
        for t in bucket.taskDict.values():
            for ev in t.eventGroups.values():
                for evCount in ev.events.values():
                    counter += evCount
        return counter
    
    def countEventsByGroupBuckets(self, name):
        counter = 0
        for bucket in buckets:
            for t in bucket.taskDict.values():
                for ev in t.eventGroups.values():
                    if ev.name == name:
                        for group, evCount in ev.events.items():
                            counter += evCount
        return counter
    
    def countEventsByGroupBucket(self, bucket, name):
        counter = 0
        for t in bucket.taskDict.values():
            for ev in t.eventGroups.values():
                if ev.name == name:
                    for group, evCount in ev.events.items():
                        counter += evCount
        return counter
    
    def listEventsCounterBuckets(self):
        d = dict()
        for bucket in buckets:
            for t in bucket.taskDict.values():
                for ev in t.eventGroups.values():
                    for group, evCount in ev.events.items():
                        if not ev.name in d:
                            d[ev.name] = dict()
                        if not group in d[ev.name]:
                             d[ev.name][group] = 0
                        d[ev.name][group] += evCount
        return d
    """
    
class Bucket():
    def __init__(self, start, end):
        self.start = start
        self.end = end
        self.taskDict = dict()
        
    def task(self, pid, tgid, name):
        if not pid in self.taskDict:
            self.taskDict[pid] = Task(pid, tgid, name)
        return self.taskDict[pid]
    
    def getTask(self, pid):
        if pid in self.taskDict:
            return self.taskDict[pid] 
        else:
            return None
        
    def count(self, pid, tgid, system, name):
        t = self.getTask(pid)
        if t is None:
            t = self.task(pid, tgid, "")
        t.countEvent(system, name)
            
    def walk(self):
        for t in self.taskDict.values():
            Collector().nextTask(t)

class Task():
    def __init__(self, pid, tgid, name):
        self.pid = pid
        self.tgid = tgid
        self.name = name
        self.eventGroups = dict()
        
    def addGroup(self, name):
        if not name in self.eventGroups:
            self.eventGroups[name] = EvGroup(name)
        return self.eventGroups[name]
            
    def getGroup(self, name):
        if name in self.eventGroups:
            return self.eventGroups[name] 
        else:
            return None          
        
    def countEvent(self, system ,name):
        g = self.getGroup(system)
        if g is None:
            g = self.addGroup(system)
        g.inc(name)
            
    def walk(self):
        for ev in self.eventGroups.values():
            for name, number in ev.events.items():
                Collector().account(ev.name, name, number)
        
class EvGroup():
    def __init__(self, name):
        self.name = name
        self.events = dict()
    
    def inc(self, eventname):
        global test
        if not eventname in self.events:
            self.events[eventname] = 0
        self.events[eventname] += 1
    
first = 1
def checkBucket(event):
    global first
    global timeIntervall
    if first:
        first = 0
        buckets.append(Bucket(event.ts, event.ts + timeIntervall * 1000000000))
    if event.ts > buckets[-1].start + timeIntervall * 1000000000:
        buckets.append(Bucket(event.ts, event.ts + timeIntervall * 1000000000))
    
def sched_event_handler(trace_seq, event):
    checkBucket(event) 
    buckets[-1].task(event.pid, event.tgid, event.name).name = event.str_field("prev_comm")
    buckets[-1].count(event.pid, event.tgid, event.system, event.name)
    print("test tgid", event.tgid)
    
def handle_all_events(trace_seq, event):
    checkBucket(event)
    buckets[-1].count(event.pid, event.tgid, event.system, event.name)
    print("test tgid", event.tgid)

def register(pevent):
    global timeIntervall
    timeIntervall = int(tep_plugin_get_option_value("bti"))
    print(timeIntervall)
    if timeIntervall == None:
        timeIntervall = 1
    pevent.register_event_handler("sched", "sched_switch", sched_event_handler)
    pevent.register_default_handler(handle_all_events)

atexit.register(print_summarry, 'Yes', 'end')

